package com.example.listmaster

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

// constructor primario ->()
class ListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val listItemId = itemView.findViewById<TextView>(R.id.item_id)
    val listItemTitle = itemView.findViewById<TextView>(R.id.item_title)

}