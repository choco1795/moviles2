package com.example.listmaster

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ListsRecyclerViewAdapter: RecyclerView.Adapter<ListViewHolder>() {
    private val mainList = arrayOf("Shopping List","Homework","Chores")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_holder, parent, false)

        return ListViewHolder(view)

    }

    override fun getItemCount(): Int {
        return mainList.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.listItemId.text = (position + 1).toString()
        holder.listItemTitle.text = mainList[position]

    }

}


/*
class Patito<T>{
    lateinit var list: Array<T>

    fun add(element: T){

    }
}

var patito = Patito<Int>()
patito.add(5)
 */